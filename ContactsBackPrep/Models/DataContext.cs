﻿using System.Data.Entity;

namespace ContactsBackPrep.Models
{
    public class DataContext : DbContext
    {
        public DataContext() : base("DefaultConnection")
        {
        }

        public System.Data.Entity.DbSet<ContactsBackPrep.Models.Contact> Contacts { get; set; }
    }
}