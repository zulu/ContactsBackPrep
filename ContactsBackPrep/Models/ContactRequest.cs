﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ContactsBackPrep.Models
{
    [NotMapped]
    public class ContactRequest : Contact
    {
        public byte[] ImageArray { get; set; }
    }
}